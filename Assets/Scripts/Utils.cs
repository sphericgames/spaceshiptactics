﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class Utils
    {

        public static bool InRange(this float value,float min, float max)
        {
            return value >= min && value <= max;
        }
        public static bool InRange(this int value, int min, int max)
        {
            return value >= min && value <= max;
        }

        /// <summary>
        /// Преобразует угол <paramref name="angle"/> в шаблон направления в дискретном поле.
        /// </summary>   
        /// <param name="angle"></param>
        /// <returns>Шаблон направления</returns>
        public static Point ToDirectionTemplate(this float angle)
        {
            switch (Mathf.RoundToInt(-angle))
            {
                case 0:
                    return new Point(0,1);
                case 45:
                case -315:
                    return new Point(1, 1);
                case 90:
                case -270:
                    return new Point(1, 0);
                case 135:
                case -225:
                    return new Point(1, -1);
                case 180:
                case -180:
                    return new Point(0, -1);
                case -45:
                case 315:
                    return new Point(-1, 1);
                case -90:
                case 270:
                    return new Point(-1, 0);
                case -135:
                case 225:
                    return new Point(-1, -1);
            }
            return new Point(0,0);
        }    

        public static Point GetPositionPoint(this Rigidbody2D rb2D)
        {            
            return new Point(Mathf.RoundToInt(rb2D.position.x), Mathf.RoundToInt(rb2D.position.y));
        }
        public static Point GetPositionPoint(this Transform transform)
        {
            return new Point(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        }

        public static void SetX(this Transform t, float x)
        {
            t.position=new Vector3(x,t.position.y,t.position.z);
        }
        public static void SetY(this Transform t, float y)
        {
            t.position = new Vector3(t.position.x, y, t.position.z);
        }  

        public static Direction ToDirection(this Quaternion q)
        {
            switch (Mathf.RoundToInt(-1 * q.eulerAngles.z))
            {
                case 0:
                    return Direction.Top;
                case 45:
                case -315:
                    return Direction.TopRight;
                case 90:
                case -270:               
                    return Direction.Right;
                case 135:
                case -225:
                    return Direction.BottomRight;
                case 180:
                case -180:
                    return Direction.Bottom;
                case -45:
                case 315:
                    return Direction.BottomLeft;
                case -90:
                case 270:
                    return Direction.Left;
                case -135:
                case 225:
                    return Direction.TopLeft;     
            }
            return Direction.Top;
        }
    }
    public enum Direction
    {
        Left = 1, Right = -1 , Top,Bottom,TopLeft,TopRight,BottomLeft,BottomRight
    }
    public enum ManeuverType
    {
        InMove, Staying
    }  

    public delegate void SelectionEventHandler(object sender);

    public interface ISelectable
    {
        event SelectionEventHandler Selected;
    }


}