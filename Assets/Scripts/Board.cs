﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Board : MonoBehaviour
    {
        #region Fields

        public List<List<Tile>> Field;
        public int Size = 16;
        public GameObject TilePrefab;
        public List<Unit> Units;
        public Unit SelectedUnit;

        public List<GameObject> UnitPrefabs;

        [SerializeField]
        public Point SpawnPosition;

        private float _objectSize = 1;

        #endregion


        void Awake()
        {
            InitializeBoard();
        }

        void Update()
        {
            if (Time.frameCount % 5 != 0) return;
        }

        #region Initialize

        private void InitializeBoard()
        {
            Field = new List<List<Tile>>();
            for (int i = 0; i < Size; i++)
            {
                List<Tile> row = new List<Tile>();
                for (int j = 0; j < Size; j++)
                {
                    GameObject instance =
                        (GameObject)
                            Instantiate(TilePrefab, new Vector3(i * _objectSize, j * _objectSize, 0), Quaternion.identity);
                    instance.name = string.Format("Tile{0}x{1}", i, j);
                    instance.transform.SetParent(transform);
                    Tile tile = instance.GetComponent<Tile>();
                    tile.Selected += OnTileSelected;
                    row.Add(tile);
                }
                Field.Add(row);
            }
            SelectedUnit = SpawnUnit(UnitPrefabs[0], SpawnPosition);
        }

        #endregion




        #region Spawn Spaceship

        private Unit SpawnUnit(GameObject spaceshipPrefab, Tile targetTile)
        {
            GameObject instance =
                (GameObject)Instantiate(spaceshipPrefab, targetTile.transform.position, Quaternion.identity);
            Unit unit = instance.GetComponent<Unit>();
            unit.Selected += OnUnitSelected;
            instance.transform.parent = transform;
            Units.Add(unit);
            return unit;
        }

        private Unit SpawnUnit(GameObject spaceshipPrefab, Point position)
        {
            return SpawnUnit(spaceshipPrefab, Field[position.X][position.Y]);
        }

        #endregion

        public void StartNextTurn()
        {
            foreach (Unit unit in Units)
            {
                unit.StartNextTurn();
            }
            StartCoroutine(ExecuteAll());

        }

        public IEnumerator ExecuteAll()
        {
            foreach (Unit unit in Units)
            {
                while (unit.MotionSystem.CanExecuteNext())
                {
                    unit.MotionSystem.Execute();
                    yield return new WaitForSeconds(0.5f);
                }
                yield return new WaitForSeconds(0.5f);
            }
        }
        public IEnumerator ExecuteAllForUnit(Unit unit)
        {
            while (unit.MotionSystem.CanExecuteNext())
            {
                unit.MotionSystem.Execute();
                yield return new WaitForSeconds(0.2f);
            }
        }

        #region EventHandlers

        private void OnUnitSelected(object sender)
        {
            SelectedUnit = (Unit)sender;
        }

        private void OnTileSelected(object sender)
        {
            if (SelectedUnit != null)
            {      
                //SelectedUnit.MotionSystem.TurnRight();
                Tile tile = (Tile) sender;
                tile.IsSelectable = true;
                SelectedUnit.MotionSystem.BuildRoute(tile.Position);
            }
        }

        #endregion



    }


}
