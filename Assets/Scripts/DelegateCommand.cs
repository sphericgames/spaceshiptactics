﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class DelegateCommand : ICommand
    {
        private readonly Action<object> _executeParamAction;
        private readonly Action<object> _undoParamAction;
        private readonly Action _executeAction;
        private readonly Action _undoAction;
        private readonly object _parameter;
        public bool CanExecute { get; set; }
        public int MpCost { get; private set; }

        public DelegateCommand(Action<object> executeParam, Action<object> undoParam, object parameter, int mpCost)
        {
            _executeParamAction = executeParam;
            _undoParamAction = undoParam;
            _parameter = parameter;
            MpCost = mpCost;
            CanExecute = true;
        }
        public DelegateCommand(Action execute, Action undo, int mpCost)
        {
            _executeAction = execute;
            _undoAction = undo;
            MpCost = mpCost;
            _parameter = null;
            CanExecute = true;
        }

        public void Execute()
        {
            if (!CanExecute)
                return;
            if(_parameter!=null)
            {
                _executeParamAction(_parameter);
            }
            else
            {
                _executeAction();
            }
        }

        public void Undo()
        {
            if (_parameter != null)
            {
                _undoParamAction(_parameter);
            }
            else
            {
                _undoAction();
            }
        }
    }
}
