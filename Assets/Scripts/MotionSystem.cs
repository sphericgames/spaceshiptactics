﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    [Serializable]
    public class MotionSystem
    {
        public readonly Rigidbody2D Rigidbody;
        public List<Point> ColliderMask;
        private Queue<ICommand> _actionQueue;
        private Stack<ICommand> _undoStack;

        private int _movementPoints;

        public int Speed { get; private set; }
        public int Agility { get; private set; }

        public int MovementPoints
        {
            get { return _movementPoints; }
            private set
            {
                if (value < 0)
                {
                    Rigidbody.SendMessageUpwards("InsufficientResource", SendMessageOptions.RequireReceiver);
                    return;
                }
                _movementPoints = value;
            }
        }

        public MotionSystem(Rigidbody2D rigidbody, int agility, int speed)
        {
            Agility = agility;
            Speed = speed;
            Rigidbody = rigidbody;
            _actionQueue = new Queue<ICommand>();
            _undoStack = new Stack<ICommand>();
        }

        #region HiAbstraction

        public virtual void MoveForward(int steps)
        {
            while (steps-- > 0)
            {
                _actionQueue.Enqueue(new DelegateCommand(StepForward, StepBackward, 1));
            }
        }

        public virtual void LeftDelayedTurn()
        {
            int steps = Agility - 1;
            while (steps-- > 0)
            {
                _actionQueue.Enqueue(new DelegateCommand(StepForward, StepBackward, 1));
            }
            _actionQueue.Enqueue(new DelegateCommand(LeftTurnWithStep, LeftTurnWithStepUndo, 0));
        }

        public virtual void RightDelayedTurn()
        {
            int steps = Agility - 1;
            while (steps-- > 0)
            {
                _actionQueue.Enqueue(new DelegateCommand(StepForward, StepBackward, 1));
            }
            _actionQueue.Enqueue(new DelegateCommand(RightTurnWithStep, RightTurnWithStepUndo, 0));
        }

        public virtual void LeftTurnAround()
        {
            _actionQueue.Enqueue(new DelegateCommand(DoNothing, DoNothing, Speed));
            _actionQueue.Enqueue(new DelegateCommand(TurnLeft, TurnRight, 0));
        }

        public virtual void RightTurnAround()
        {
            _actionQueue.Enqueue(new DelegateCommand(DoNothing, DoNothing, Speed));
            _actionQueue.Enqueue(new DelegateCommand(TurnRight, TurnLeft, 0));
        }

        #endregion
                         
        #region monatomic actions

        private void LeftTurnWithStep()
        {
            TurnLeft();
            StepForward();
        }
        private void LeftTurnWithStepUndo()
        {
            StepBackward();
            TurnRight();
        }
        private void RightTurnWithStep()
        {
            TurnRight();
            StepForward();
        }
        private void RightTurnWithStepUndo()
        {
            StepBackward();
            TurnLeft();
        }

        private void StepBackward()
        {
            Point dir = Rigidbody.rotation.ToDirectionTemplate();
            Rigidbody.MovePosition(Rigidbody.position - dir.ToVector2());
        }

        private void StepForward()
        {
            Point dir = Rigidbody.rotation.ToDirectionTemplate();
           Rigidbody.position += dir.ToVector2();
        }

        private void TurnLeft()
        {
            Turn(45);
        }

        private void TurnRight()
        {
            Turn(-45);
        }

        private void Turn(int angle)
        {
            Rigidbody.rotation += angle;
            if (Rigidbody.rotation >= 360)
            {
                Rigidbody.rotation -= 360;
            }
            else if (Rigidbody.rotation <= -360)
            {
                Rigidbody.rotation += 360;
            }
        }

        private void DoNothing()
        {
        }

        #endregion
                          
        public virtual void StartNextTurn()
        {
            _undoStack.Clear();
            MovementPoints = Speed;
        }

        #region execution

        public void Undo()
        {
            if (_undoStack.Count <= 0) return;
            ICommand command = _undoStack.Pop();
            MovementPoints += command.MpCost;
            _actionQueue.Clear();
        }

        public void Execute()
        {
            if (_actionQueue.Count > 0)
            {

                if (_actionQueue.Peek().CanExecute && _actionQueue.Peek().MpCost <= MovementPoints)
                {
                    ICommand command = _actionQueue.Dequeue();
                    command.Execute();
                    MovementPoints -= command.MpCost;
                    _undoStack.Push(command);
                }
                else
                {
                    Rigidbody.SendMessageUpwards("ActionUnavailableNow", SendMessageOptions.RequireReceiver);
                }
            }

        }

        public bool CanExecuteNext()
        {
            return (_actionQueue.Count > 0 && _actionQueue.Peek().CanExecute &&
                    _actionQueue.Peek().MpCost <= MovementPoints);
        }

        #endregion     

        #region routing

        public void BuildRoute(Point position)
        {
            Point routePoint = Rigidbody.GetPositionPoint();
            float myAngle = Rigidbody.rotation;
            _actionQueue.Clear();
            while (position != routePoint)
            {
                myAngle = myAngle%360;
                int angleToTarget = Mathf.RoundToInt(Vector2.Angle(myAngle.ToDirectionTemplate().ToVector2(),
                position.ToVector2() - routePoint.ToVector2()));
                float distance = Vector2.Distance(routePoint.ToVector2(), position.ToVector2());
                if (angleToTarget >= 45)
                {
                    if (Vector2.Angle((myAngle - 45).ToDirectionTemplate().ToVector2(),
                        position.ToVector2() - routePoint.ToVector2()) < angleToTarget)
                    {
                        RightTurnAround();
                        myAngle -= 45;
                    }
                    else
                    {
                        LeftTurnAround();
                        myAngle += 45;
                    }
                }
                else if (angleToTarget==0)
                {
                    int steps = Mathf.RoundToInt(distance);
                    MoveForward(steps);
                    routePoint += myAngle.ToDirectionTemplate()*steps;
                }
                else 
                {
                    if (Mathf.RoundToInt(Vector2.Angle((myAngle).ToDirectionTemplate().ToVector2(),
                        (position).ToVector2() - (routePoint + myAngle.ToDirectionTemplate() * (Agility - 1)).ToVector2())) == 45)
                    {
                        if (Mathf.RoundToInt(Vector2.Angle((myAngle - 45).ToDirectionTemplate().ToVector2(),
                            (position).ToVector2() -
                            (routePoint + myAngle.ToDirectionTemplate()*(Agility - 1)).ToVector2())) < 45)
                        {
                            RightTurnAround();
                            routePoint += myAngle.ToDirectionTemplate() * (Agility-1);
                            myAngle -= 45;
                            routePoint += myAngle.ToDirectionTemplate();
                        }
                        else
                        {
                            LeftTurnAround();
                            routePoint += myAngle.ToDirectionTemplate() * (Agility - 1);
                            myAngle += 45;
                            routePoint += myAngle.ToDirectionTemplate();
                        }
                    }
                    else
                    {
                        MoveForward(1);
                        routePoint += myAngle.ToDirectionTemplate();
                    }
                }

            }
        }

        public void CancelRoute()
        {
            _actionQueue.Clear();
        }

        #endregion

    }

    public interface ICommand
    {
        bool CanExecute { get; set; }
        int MpCost { get; }
        void Execute();
        void Undo();
    }
}
