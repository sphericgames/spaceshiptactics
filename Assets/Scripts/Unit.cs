﻿using System.Collections;
using System.Diagnostics;        
using UnityEngine;
using UnityEngine.EventSystems;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts
{
    public class Unit : MonoBehaviour, IPointerClickHandler, ISelectable
    {
        public MotionSystem MotionSystem;

        void Awake()
        {
            MotionSystem=new MotionSystem(GetComponent<Rigidbody2D>(),4,3);
        }
             

        #region Selected Event

        public event SelectionEventHandler Selected;

        public void OnPointerClick(PointerEventData eventData)
        {
            OnSelected();   
        }

        protected virtual void OnSelected()
        {
            if (Selected != null) Selected(this);
        }

        #endregion

        public void StartNextTurn()
        {
            MotionSystem.StartNextTurn();
        }
    }
}
