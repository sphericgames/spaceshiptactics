﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour {

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}
