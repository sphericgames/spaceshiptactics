﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CameraMovement : MonoBehaviour
    {
        public enum MouseButtons
        {
            Left = 0,
            Right,
            Wheel,
            Button4,
            Button5,
            Button6
        };

        private Board _board;

        public int ZoomMin = 5;
        public int ZoomMax = 70;
        public MouseButtons MoveButton = MouseButtons.Left;
        public bool DebuggingMode = false;

        private bool _isMoving = false;
        private Vector3 _startMousePosition;
        private Vector3 _startCameraPosition;

        void Awake()
        {
            _board=GameObject.Find("Board").GetComponent<Board>();
            transform.position=new Vector3(_board.Size/2,_board.Size/2,transform.position.z);
        }

        void LateUpdate()
        {
            if (Input.GetMouseButton((int)MoveButton) && !_isMoving)
            {
                StartMovement();
            }
            if (!Input.GetMouseButton((int)MoveButton) && _isMoving)
            {
                EndMovement();
            }

            if (_isMoving)
            {
                MoveCamera();
            }    
            HandleZoom();

        }

        private void HandleZoom()
        {
            float delta = Input.mouseScrollDelta.y;
            if (Camera.main.orthographicSize + delta < ZoomMin)
            {
                Camera.main.orthographicSize = ZoomMin;
                return;
            }
            if (Camera.main.orthographicSize + delta > ZoomMax)
            {
                Camera.main.orthographicSize = ZoomMax;
                return;
            }
            Camera.main.orthographicSize += delta;
        }

        private void MoveCamera()
        {
            Vector3 newMousePosition = Input.mousePosition;
            Vector3 mouseDelta = Camera.main.ScreenToWorldPoint(_startMousePosition) - Camera.main.ScreenToWorldPoint(newMousePosition);
            Vector3 targetVector = _startCameraPosition + mouseDelta;        
            transform.Translate(targetVector - transform.position,Space.World);
            AdjustPosition();
        }

        private void AdjustPosition()
        {
            if (transform.position.x < 0)
            {
                transform.SetX(0);
            }
            else if (transform.position.x > _board.Size)
            {
                transform.SetX(_board.Size);
            }
            if (transform.position.y < 0)
            {
                transform.SetY(0);
            }
            else if (transform.position.y > _board.Size)
            {
                transform.SetY(_board.Size);
            }
        }

        private void EndMovement()
        {
            _isMoving = false;
            if(DebuggingMode)
            {
                Debug.Log("================END=================");
            }
        }

        private void StartMovement()
        {
            _isMoving = true;
            _startMousePosition = Input.mousePosition;
            _startCameraPosition = transform.position;
        }
    }
}
