﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts
{
    public class Tile : MonoBehaviour,IPointerEnterHandler,IPointerClickHandler,IPointerExitHandler,ISelectable
    {
        public Point Position
        {
            get {return transform.GetPositionPoint(); }
        }

        public bool DebuggingMode = false;
        private bool _isSelectable = false;
        public bool IsMouseover = false;
        public event SelectionEventHandler Selected;
        public Color SelectableColor;
        public Color MouseoverColor;
        public Color SelectableMouseoverColor;
        public Color DefaultColor;

        private SpriteRenderer _spriteRenderer;   

        public bool IsSelectable
        {
            get { return _isSelectable; }
            set
            {
                _isSelectable = value; 
                UpdateColor();
            }
        }

        void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        } 

        public void OnPointerClick(PointerEventData eventData)
        {
            if(DebuggingMode)
            {
                Debug.Log(Position.X+ "x" + Position.Y);
                Debug.Log("Objects:"+eventData.hovered.Count);
            }
            if (_isSelectable)
            {
                OnSelected();
            } 
                OnSelected();  
        }

        public void UpdateColor()
        {
            if (IsMouseover && _isSelectable)
            {
                _spriteRenderer.color = SelectableMouseoverColor;  
            }
            else if (IsMouseover)
            {
                _spriteRenderer.color = MouseoverColor;
            }
            else if (_isSelectable)
            {
                _spriteRenderer.color = SelectableColor;
            }
            else
            {
                _spriteRenderer.color = DefaultColor;
            }
                
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            IsMouseover = true;
            UpdateColor();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            IsMouseover = false;
            UpdateColor();
        }

        protected virtual void OnSelected()
        {
            if (Selected != null) Selected.Invoke(this);
        }
    }
}
