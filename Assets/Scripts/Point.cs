﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class Point
    {
        protected bool Equals(Point other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Point) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X*397) ^ Y;
            }
        }

        public int X;
        public int Y;

        public Point(int x,int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return String.Format("({0}:{1})",X,Y);
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X,Y);
        }

        public static bool operator ==(Point a, Point b)
        {
            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
            {
                return false;
            }
            return  (a.X == b.X && a.Y == b.Y);
        }

        public static bool operator !=(Point a, Point b)
        {
            return !(a == b);
        }

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.X+b.X,a.Y+b.Y);
        }
        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }
        public static Point operator *(Point a, int b)
        {
            return new Point(a.X*b, a.Y*b);
        }
    }
}
